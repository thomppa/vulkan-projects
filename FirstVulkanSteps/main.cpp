#include <cstdio>
#include <glm/ext/matrix_float2x4.hpp>
#include <glm/ext/matrix_float4x4.hpp>
#include <glm/ext/vector_float4.hpp>
#include <sys/types.h>
#include <vulkan/vulkan_core.h>
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>

#include <iostream>


int main()
{
  glfwInit();

  glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

  GLFWwindow * window = glfwCreateWindow(800, 600, "Test Window", nullptr, nullptr);

  u_int32_t extensionCount = 0;
  vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);

  printf("Extensioncount: %i\n", extensionCount);
  
  glm::mat4 matrix;
  glm::vec4 vec;

  auto test = matrix * vec;

  while(!glfwWindowShouldClose(window))
    {
      glfwPollEvents();
    }

  glfwDestroyWindow(window);

  glfwTerminate();

  return 0;
}
