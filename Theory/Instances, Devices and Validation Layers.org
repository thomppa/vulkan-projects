* Instances, Devices and Validation Layers

Setting up Vulkan to use a device, and enabling Validation Layers to validate code!

** Vulkan Instance

- A Vulkan Instance is a reference to the vulkan context

- Defines the Vulkan version , and its capabilities.

- All Vulkan applications start by creating a Vulkan Instance

- Physical Devices accessible to the Instance are enumerated and one or more are chosen...

- Then the Instance creates a Logical Device to handle the rest of the work!

- Instaces are rarely used beyond this.

** Device

*** Physical Device

*The GPU itself. Holds memory and queues to process pipeline, but can't be interacted with directly.*

Contains two important aspects:

  - Memory: When we want to allocate memory to resources, it must be handled through the Physical Device.

  - Queues: Process commands submitted to GPU in FIFO (First in First Out) order. Different  queues can be used for different types of command.

*Physical Devices are "retrieved" not created like most Vulkan concepts (One can't create a physical concept out of thin air)*

*Do this by enumerating over all devices and picking suitable one.*

**** Queues

Physical Devices can have multiple types of queues.

The types are reffered to as "Queue Families".

A Family can have more than one queue in it.

- Example Queue Families:
  - *Graphics*: A family for processing graphics commands.
  - *Compute*: A family for processing compute shaders (generic commands)
  - *Transfer*: A family for processing data transfer operations.

 *Queue families can often be a combination of these!*

 *When we enumerate Physical Devices, we need to check if the device has the queue families we require for the application.*
  
*** Logical Device

*An interface to the Physical Device. This will be used a lot.
Through Logical Device we set up everything on the GPU.* 

It will be referenced a lot in Vulkan functions.

Most Vulkan objects are created on the device, and we use the reference to the Logical Device to state which device to create those objects on.

Creation is relatively simple:

- define queue families and number of queues you wish to assign to the Logical Device from the Physical Device.
- define all the features you wish to enable (e.g. Geometry Shader, anisotropy, wide lines, etc)
- define extensions the device will use.
- In the past you would define Validation Layers too. As of Vulkan 1.1, this is deprecated.

** Extensions

*By default, Vulkan has no understanding of what a ""window"" is.*

That makes sense since Vulkan is cross-platform and windows are defined differently on different systems.

*So Vulkan uses extensions to define window functionality.*

These extensions are so commonly used, that they come pre-packaged with vulkan anyway!

We can choose required extensions manually, but GLFW library has functions to choose them for us!

** GLFW

*Glfw is the "Graphics Library Framework".*

Originally designed for OpenGL but updated to work with Vulkan.

Allows cross-platform window creation and automatic interfacing with OpenGL/Vulkan.

Contains function that identifies the required Vulkan extensions for the host system, and returns a list of them.

*glfwGetRequiredInstaceExtensions()*

*It can then use this list to set uo Vulkan with the correct extensions!*

** Validation Layers

By default, Vulkan does not validate code. Will not report errors, and will simply crash if it encounters a fatal error.

*This is to avoid unnecessary overhead of error checking in release code*

We must enable a Validation "Layer" to check.

Each "Layer" can check different functions.

 *Layers are similar to extensions and are not built-in to the core Vulkan code. They must be acquired from third parties*

 *Additionally, the reporting of validation errors is not a core Vulkan function and will requie another extension to be applied.*

* Summary

- Vulkan applications start by creating a Vulkan Instance that defines the Vulkan application.
- Enumerate Physical Device to pick an adequate GPU
- Create Logical Device to interface with chosen Physical Device
- Multiple types of queue (queue families) on a Physical Device can be assigned to a Logical Device. Each one processes different types of command.
- Extensions can be applied, such as extensions to enable displaying of a window.
- GLFW is an extension to create our windows and choose appropriate extensions.
- Validation Layers are optional and allow to validate the code at run-time. They can be disabled for release versions to reduce overhead.
